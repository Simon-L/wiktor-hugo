package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gohugoio/hugo/parser"
	"github.com/gohugoio/hugo/parser/metadecoders"
	"github.com/gohugoio/hugo/parser/pageparser"
)

var wikiContentRoot = "./content/wiki/"

func ifErr500(err error, w http.ResponseWriter, r *http.Request) {
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "http://localhost:1313/500#"+err.Error(), 301)
		return
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	log.Println("-------------------------------------------------")
	log.Println("")
	err := r.ParseForm()
	ifErr500(err, w, r)

	// Parse form values
	var isRenamed = false
	if r.Form["isRenamed"][0] == "true" {
		isRenamed = true
	}
	var isCategoryChanged = false
	if r.Form["isCategoryChanged"][0] == "true" {
		isCategoryChanged = true
	}

	// Set header to accept different origin request
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:1313")

	// Page update, has "fs" parameter for file on disk
	if len(r.Form["fs"][0]) > 0 {
		log.Println("Form:")
		log.Println(r.Form)
		file := wikiContentRoot + r.Form["fs"][0]
		log.Println(file)
		f, err := os.OpenFile(file, os.O_RDWR, 0700)
		ifErr500(err, w, r)

		// Parse existing page from file
		p, err := pageparser.Parse(f)
		ifErr500(err, w, r)
		md, err := metadecoders.UnmarshalToMap(p.Iterator().Peek().Val, metadecoders.YAML)
		content := p.Input()[1:]
		ifErr500(err, w, r)

		// Category has changed
		if isCategoryChanged {
			log.Println("isCategoryChanged: ", isCategoryChanged)
			md["category"] = r.Form["pageCategory"][0]
		}

		// Title has changed
		if isRenamed {
			log.Println("isRenamed: ", isRenamed)
			md["title"] = r.Form["pageTitle"][0]
		}

		// Truncate to make the file empty
		f.Truncate(0)
		// Seek to beginning of file
		f.Seek(0, 0)
		// Write FrontMatter to file (using parser from hugo package)
		err = parser.InterfaceToFrontMatter(md, metadecoders.YAML, f)
		ifErr500(err, w, r)

		// Content has changed
		if bytes.Compare(content, []byte(r.Form["pageContent"][0])) != 0 {
			log.Println("Changed to:")
			fmt.Println(r.Form["pageContent"][0])
			_, err = f.Write([]byte(r.Form["pageContent"][0]))
			ifErr500(err, w, r)
		} else {
			log.Println("Not changed")
			_, err = f.Write(content)
			ifErr500(err, w, r)
			fmt.Println(string(content))
		}

		// Close file when writing is done
		f.Close()
		// Move (rename) file if title has changed
		if isRenamed {
			os.Rename(file, wikiContentRoot+r.Form["pageTitle"][0]+".md")
			log.Println("1")
			time.Sleep(1 * time.Second)
			http.Redirect(w, r, "http://localhost:1313/wiki/"+strings.ToLower(r.Form["pageTitle"][0])+"/", 301)
		} else {
			log.Println("2")
			log.Println(r.FormValue("url"))
			time.Sleep(1 * time.Second)
			http.Redirect(w, r, "http://localhost:1313/"+r.FormValue("url")+"/", 301)
		}
	} else {
		// New page
		var p bytes.Buffer
		// Write FrontMatter to file (using parser from hugo package)
		err = parser.InterfaceToFrontMatter(map[string]string{"title": r.Form["pageTitle"][0], "category": r.Form["pageCategory"][0]}, metadecoders.YAML, &p)
		ifErr500(err, w, r)

		_, err = p.WriteString(r.Form["pageContent"][0])
		ifErr500(err, w, r)

		ioutil.WriteFile(wikiContentRoot+r.Form["pageTitle"][0]+".md", p.Bytes(), 0700)
		log.Println("3")
		time.Sleep(1 * time.Second)
		http.Redirect(w, r, "http://localhost:1313/wiki/"+strings.ToLower(r.Form["pageTitle"][0])+"/", 301)
	}
}

func main() {
	http.HandleFunc("/a/save", handler)
	srv := &http.Server{
		Addr:         ":1414",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	log.Println("Serving...")
	log.Fatal(srv.ListenAndServe())
}
