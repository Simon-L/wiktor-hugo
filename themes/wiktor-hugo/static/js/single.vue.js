Vue.use(Buefy.default)

var app = new Vue({
  el: '#main',
  data: {
    message: 'Hello Vue.js!',
    a: 43
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  },
  created: function () {
    // `this` points to the vm instance
    console.log('a is: ' + this.a)
  }
})
