Vue.use(Buefy.default)
Vue.use(VueResource)

var app = new Vue({
  delimiters: ['[[', ']]'],
  el: '#main',
  data: {
    currentPage: {},
    isLoading: true,
    pageTitle: "",
    pageCategory: "",
    pageHash: "",
    isMessageShown: false,
    message: {
      text: "",
      type: ""
    }
  },
  mounted: function () {
    this.pageHash = decodeURI(window.location.hash.substring(1))
    var simplemde = new EasyMDE({ element: document.getElementById("editorarea") })
    if (this.pageHash === "") {
      // Show new page message
      this.isLoading = false
      this.showMessage("You are creating a new page!", "success")
    } else {
      this.$http.get(this.pageHash + '/index.json').then(response => {
        this.isLoading = false
        simplemde.value(response.body.content)
        this.currentPage = response.body
        this.pageTitle = this.currentPage.params.title
        this.pageCategory = this.currentPage.params.category
        console.log("Done!")
      }, response => {
        this.pageTitle = this.pageHash
        this.isLoading = false
        this.showMessage("The page could not be found, you are creating a new page!", "warning")
      })
    }
  },
  methods: {
    showMessage(text, type) {
      this.message.text = text
      this.message.type = "is-"+type
      this.isMessageShown = true
    },
    haschanged(editor, page) {
      if (this.currentPage.params) return editor !== this.currentPage.params[page]
      else return false
    }
  }
})
