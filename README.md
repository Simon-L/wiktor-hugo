# Wiktor Hugo

> A writable wiki based on Hugo!

Wiktor is two things:
* A hugo theme: `themes/wiktor-hugo`
* An api in `api.go` that enables writing to the hugo website content

Requirements: https://gohugo.io and https://caddyserver.com

A major overhaul is coming soon, for future reference, current pre-alpha-ish state requires three terminals:

```
$ hugo server

$ caddy

$ go run api.go
```
open the link given by caddy
